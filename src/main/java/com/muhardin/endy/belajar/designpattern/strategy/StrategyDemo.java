package com.muhardin.endy.belajar.designpattern.strategy;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

public class StrategyDemo {
    public static void main(String[] args) {
        Produk p001 = new Produk();
        p001.setKode("M-001");
        p001.setNama("Mouse Logitech");
        p001.setHarga(new BigDecimal(500000));

        Produk p002 = new Produk();
        p002.setKode("L-001");
        p002.setNama("Laptop Lenovo");
        p002.setHarga(new BigDecimal(15000000));

        PembelianDetail pd1 = new PembelianDetail();
        pd1.setProduk(p001);
        pd1.setJumlah(2);

        PembelianDetail pd2 = new PembelianDetail();
        pd2.setProduk(p002);
        pd2.setJumlah(1);

        Pembelian pembelian = new Pembelian();
        pembelian.setNamaPembeli("Agus");
        pembelian.setWaktuTransaksi(LocalDateTime.now());
        pembelian.getDaftarPembelian().add(pd1);
        pembelian.getDaftarPembelian().add(pd2);

        // menambahkan diskon
        DiskonTotal dt = new DiskonTotal(new BigDecimal(10000000), new BigDecimal(0.1));
        pembelian.getDaftarDiskon().add(dt);

        // daftar diskon yang ditambahkan ke pembelian
        // nantinya bisa diambil dari UI, misalnya dengan checkbox
        // atau dari setting parameter/konfigurasi
        DiskonProduk dp = new DiskonProduk("Lenovo", new BigDecimal(0.05));
        pembelian.getDaftarDiskon().add(dp);

        System.out.println("Total pembelian : "+pembelian.total().setScale(2, RoundingMode.HALF_EVEN));
        System.out.println("Total diskon : "+pembelian.totalDiskon().setScale(2, RoundingMode.HALF_EVEN));
        System.out.println("Yang harus dibayar : "+pembelian.dibayar().setScale(2, RoundingMode.HALF_EVEN));
    }
}
