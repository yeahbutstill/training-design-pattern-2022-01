package com.muhardin.endy.belajar.designpattern.state;

public class StateMenungguPembayaran implements StatePembelian{

    @Override
    public void kirimNotifikasi() {
        System.out.println("Mohon segera lakukan pembayaran");
    }
    
}
