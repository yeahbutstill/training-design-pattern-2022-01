package com.muhardin.endy.belajar.designpattern.memento;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor @Getter
public class ProdukSnapshot {
    private LocalDateTime waktuSnapshot = LocalDateTime.now();
    private String kode;
    private String nama;
    private String foto;
    private BigDecimal harga;
}
