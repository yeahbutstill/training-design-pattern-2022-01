package com.muhardin.endy.belajar.designpattern.strategy;

import java.math.BigDecimal;

public interface Diskon {
    public BigDecimal hitung(Pembelian pembelian);
}
