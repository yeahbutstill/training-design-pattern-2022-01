package com.muhardin.endy.belajar.designpattern.observer;

import java.awt.event.*;
import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalculatorDemo {
    public static void main(String[] args) {
        JFrame fr = new JFrame("Kalkulator");
        fr.setSize(800, 600);
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel panel = new JPanel();

        // TODO : masking supaya cuma bisa input angka
        JTextField txtNum1 = new JTextField(5);
        JTextField txtNum2 = new JTextField(5);
        JLabel lblPlus = new JLabel("+");
        JButton btnEq = new JButton("=");
        JTextField txtHasil = new JTextField(5);
        txtHasil.setEditable(false);

        panel.add(txtNum1);
        panel.add(lblPlus);
        panel.add(txtNum2);
        panel.add(btnEq);
        panel.add(txtHasil);

        fr.getContentPane().add(panel, BorderLayout.CENTER);
        fr.pack();
        fr.setLocationRelativeTo(null);
        fr.setVisible(true);

        btnEq.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                Integer num1 = Integer.parseInt(txtNum1.getText());
                Integer num2 = Integer.parseInt(txtNum2.getText());
                Integer hasil = num1 + num2;
                txtHasil.setText(hasil.toString());
            }

        });

        btnEq.addActionListener(e -> System.out.println("Action listener kedua"));

        btnEq.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseEntered(MouseEvent e) {
                System.out.println("Mouse entered");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                System.out.println("Mouse Exited");
            }
            
        });
    } 
}
