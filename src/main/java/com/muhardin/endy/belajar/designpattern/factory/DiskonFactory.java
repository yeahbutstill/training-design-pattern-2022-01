package com.muhardin.endy.belajar.designpattern.factory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.muhardin.endy.belajar.designpattern.strategy.Diskon;
import com.muhardin.endy.belajar.designpattern.strategy.DiskonProduk;
import com.muhardin.endy.belajar.designpattern.strategy.DiskonTotal;

import lombok.Builder;

@Builder
public class DiskonFactory {

    @Builder.Default
    private Boolean enableDiskonProduk = false;
    @Builder.Default
    private Boolean enableDiskonTotal = false;

    private BigDecimal batasDiskonTotal;
    private List<String> daftarProdukDiskon;

    @Builder.Default
    private BigDecimal persentaseDiskonProduk = BigDecimal.ZERO;
    @Builder.Default
    private BigDecimal persentaseDiskonTotal = BigDecimal.ZERO;

    public List<Diskon> getActiveDiskon(){
        List<Diskon> hasil = new ArrayList<>();
        if(enableDiskonProduk){
            for(String namaProduk : daftarProdukDiskon) {
                DiskonProduk dp = new DiskonProduk(namaProduk, persentaseDiskonProduk);
                hasil.add(dp);
            }
        }
        if(enableDiskonTotal && batasDiskonTotal != null){
            DiskonTotal dt = new DiskonTotal(batasDiskonTotal, persentaseDiskonTotal);
            hasil.add(dt);
        }
        return hasil;
    }
}
