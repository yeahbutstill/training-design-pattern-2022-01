package com.muhardin.endy.belajar.designpattern.interceptor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.muhardin.endy.belajar.designpattern.command.BankingService;
import com.muhardin.endy.belajar.designpattern.command.SetoranCommand;

public class InterceptorDemo {
    public static void main(String[] args) {
        BankingService service = new BankingService();
        SetoranCommand cmdSetor = new SetoranCommand("Nasabah 99", new BigDecimal(25000000));
        
        List<BankingInterceptor> daftarInterceptor = new ArrayList<>();
        daftarInterceptor.add(new CekRekeningInterceptor());
        daftarInterceptor.add(new NilaiSetoranInterceptor(new BigDecimal(10000000)));

        BankingProxy proxy = new BankingProxy(daftarInterceptor, service);
        proxy.execute(cmdSetor);
    }
}
