package com.muhardin.endy.belajar.designpattern.state;

public interface StatePembelian {
    public void kirimNotifikasi();
}
