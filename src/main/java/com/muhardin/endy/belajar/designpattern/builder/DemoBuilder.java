package com.muhardin.endy.belajar.designpattern.builder;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.muhardin.endy.belajar.designpattern.prototype.Alamat;
import com.muhardin.endy.belajar.designpattern.prototype.Customer;

public class DemoBuilder {
    public static void main(String[] args) {
        Produk p = new Produk();
        p.setKode("P-001");
        p.setNama("Produk 001");
        p.setHarga(new BigDecimal("100000.03"));
        displayProduk(p);

        Produk p2 = new Produk();
        //displayProduk(p2);

        Produk p3 = new Produk();
        p3.setHarga(new BigDecimal("-125000"));

        Produk p4 = new ProdukBuilder()
                    .kode("P-004")
                    .nama("Produk 004")
                    .harga(new BigDecimal("0"))
                    .build();
        displayProduk(p4);

        Alamat a = new Alamat();
        Customer c1 = Customer.builder()
                    //.nama("Customer 001")
                    .alamat(a)
                    .email("cust001@yopmail.com")
                    .build();
                    
    }

    public static void displayProduk(Produk x){
        System.out.println("Kode : "+x.getKode());
        System.out.println("Nama : "+x.getNama());
        System.out.println("Harga : "+x.getHarga().setScale(0, RoundingMode.HALF_EVEN));
    }
}
