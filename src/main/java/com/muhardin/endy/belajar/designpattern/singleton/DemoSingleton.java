package com.muhardin.endy.belajar.designpattern.singleton;

public class DemoSingleton {
    public static void main(String[] args) {
        KoneksiDatabase kd1 = new KoneksiDatabase("belajardb", "belajar", "java");
        kd1.connect();

        KoneksiDatabase kd2 = new KoneksiDatabase("belajardb", "belajar", "java");
        kd2.connect();

        KoneksiDatabase kd3 = new KoneksiDatabase("belajardb", "belajar", "java");
        kd3.connect();

        KoneksiDatabaseSingleton kds1 = KoneksiDatabaseSingleton.getKoneksiDatabase("belajardb", "belajar", "java");
        kds1.connect();

        KoneksiDatabaseSingleton kds2 = KoneksiDatabaseSingleton.getKoneksiDatabase("belajardb", "belajar", "java");
        kds2.connect();

        KoneksiDatabaseSingleton kds3 = KoneksiDatabaseSingleton.getKoneksiDatabase("belajardb", "belajar", "java");
        kds3.connect();
    }
}
