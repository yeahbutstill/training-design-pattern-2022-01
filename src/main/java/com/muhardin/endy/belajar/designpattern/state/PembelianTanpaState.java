package com.muhardin.endy.belajar.designpattern.state;

import java.time.LocalDateTime;

public class PembelianTanpaState {
    private StatusPembelian status;
    private String pembeli;
    private LocalDateTime waktuTransaksi;

    public void kirimNotifikasi(){
        switch(status) {
            case MENUNGGU_PEMBAYARAN: 
                System.out.println("Mohon segera lakukan pembayaran");
                break;
            case PEMBAYARAN_DITERIMA:
                System.out.println("Terima kasih atas pembayaran");
            default:
                System.out.println("Belum diimplementasikan");
        }
    }
}
