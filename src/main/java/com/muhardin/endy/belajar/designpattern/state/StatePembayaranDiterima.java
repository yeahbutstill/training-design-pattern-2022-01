package com.muhardin.endy.belajar.designpattern.state;

public class StatePembayaranDiterima implements StatePembelian {

    @Override
    public void kirimNotifikasi() {
        System.out.println("Terima kasih atas pembayaran");
    }
    
}
