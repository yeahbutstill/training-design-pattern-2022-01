package com.muhardin.endy.belajar.designpattern.templatemethod;

public class TemplateMethodDemo {
    public static void main(String[] args) {
        CustomerReportCsv customerReportCsv = new CustomerReportCsv("customer-data.csv");
        customerReportCsv.generateReport();
    }
}
