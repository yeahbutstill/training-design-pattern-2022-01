package com.muhardin.endy.belajar.designpattern.interceptor;

import java.util.List;

import com.muhardin.endy.belajar.designpattern.command.BankingCommand;
import com.muhardin.endy.belajar.designpattern.command.BankingService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class BankingProxy {
    private List<BankingInterceptor> interceptors;
    private BankingService targetObject;

    public void execute(BankingCommand cmd){
        for(BankingInterceptor bi : interceptors){
            bi.process(cmd);
        }
        targetObject.execute(cmd);
    }
}
