package com.muhardin.endy.belajar.designpattern.templatemethod;

import lombok.Data;

@Data
public class Customer {
    private String id;
    private String kode;
    private String nama;
    private String email;
}
