package com.muhardin.endy.belajar.designpattern.templatemethod;

public abstract class ReportGenerator {
    
    public final void generateReport(){
        fetchData();
        validate();
        render();
    }

    protected abstract void fetchData();
    protected abstract void validate();
    protected abstract void render();
}
