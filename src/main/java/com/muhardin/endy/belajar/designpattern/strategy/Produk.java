package com.muhardin.endy.belajar.designpattern.strategy;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Produk {
    private String kode;
    private String nama;
    private BigDecimal harga;
}
