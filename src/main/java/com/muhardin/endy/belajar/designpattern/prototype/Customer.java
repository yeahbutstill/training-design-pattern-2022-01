package com.muhardin.endy.belajar.designpattern.prototype;

import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import lombok.NonNull;

@Builder
public class Customer implements Cloneable {

    @NonNull
    private String nama;
    private String email;
    private Alamat alamat;

    private List<String> telepon;

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getNama(){
        return this.nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getTelepon() {
        return telepon;
    }

    public void setTelepon(List<String> telepon) {
        this.telepon = telepon;
    }

    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }

    public Alamat getAlamat() {
        return alamat;
    }

    public void setAlamat(Alamat alamat) {
        this.alamat = alamat;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nama == null) ? 0 : nama.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((alamat == null) ? 0 : alamat.hashCode());
        result = prime * result + ((telepon == null) ? 0 : telepon.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Customer other = (Customer) obj;
        if (nama == null) {
            if (other.nama != null)
                return false;
        } else if (!nama.equals(other.nama))
            return false;
        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        if (alamat == null) {
            if (other.alamat != null)
                return false;
        } else if (!alamat.equals(other.alamat))
            return false;
        if (telepon == null) {
            if (other.telepon != null)
                return false;
        } else if (!telepon.equals(other.telepon))
            return false;
        return true;
    }


    
}
