package com.muhardin.endy.belajar.designpattern.command;

import java.math.BigDecimal;

import lombok.Getter;

@Getter
public class SetoranCommand implements BankingCommand {

    private String nasabah;
    private BigDecimal nilai;

    public SetoranCommand(String nasabah, BigDecimal nilaiSetoran) {
        this.nasabah = nasabah;
        this.nilai = nilaiSetoran;
    }

    @Override
    public void run() {
        System.out.println("Nasabah "+nasabah+" melakukan setoran senilai "+nilai);
    }

}
