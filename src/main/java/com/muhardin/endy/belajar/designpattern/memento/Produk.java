package com.muhardin.endy.belajar.designpattern.memento;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class Produk {
    private String kode;
    private String nama;
    private String foto;
    private BigDecimal harga;
    
    public ProdukSnapshot saveSnapshot(){
        return new ProdukSnapshot(LocalDateTime.now(), kode, nama, foto, harga);
    }

    public void restoreSnapshot(ProdukSnapshot snapshot){
        this.kode = snapshot.getKode();
        this.nama = snapshot.getNama();
        this.foto = snapshot.getFoto();
        this.harga = snapshot.getHarga();
    }
}
